﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QrScanLib
{
    internal class PixelScanResult
    {
        public PixelScanResult(float x, float y, float s)
        {
            X = x;
            Y = y;
            Size = s;
            Weight = 1.0f;
        }

        public float X;
        public float Y;
        public float Size;

        public float Weight;
    }
}
