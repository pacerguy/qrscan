﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;

namespace QrScanLib
{
    public class Scanner
    {
        private PixelScanner AnchorScanner { get; set; }
        private PixelScanner AlignmentScanner { get; set; }
        private PixelScanner TimingScanner { get; set; }

        private float Variance { get; set; }

        public Scanner(float variance)
        {
            if (variance <= 0.0f) throw new ArgumentException("Variance must be greater than zero.");

            Variance = variance;
            AnchorScanner = new PixelScanner(new float[] { 1.0f, 1.0f, 3.0f, 1.0f, 1.0f }, 0, Variance);
            AlignmentScanner = new PixelScanner(new float[] { 1.0f, 1.0f, 1.0f }, 1, Variance);
            TimingScanner = new PixelScanner(new float[] { 1.0f }, 1, Variance);
        }

        // Perform a scan.  Expects an array of 0 (black) and 1 (white).
        public void Scan(ScanState state)
        {
            if (state == null) throw new ArgumentNullException();

            state.AnchorPoints = PerformScan(state, AnchorScanner);
            ProcessAnchors(state);
            CalculateTimingPoints(state);
            CalculateSizeFromTiming(state);
            state.AlignmentPoints = PerformScan(state, AlignmentScanner);

            ReadCode(state);
        }

        private Vector2[] PerformScan(ScanState state, PixelScanner scanner)
        { 
            // Find all possible matches, horizontally
            List<PixelScanResult> firstPass = new List<PixelScanResult>();
            for(int i = 0; i < state.Height; i++)
            {
                scanner.ScanLine(state.Pixels, 0, i, 1, 0, state.Width-1, state.Height-1, firstPass);
            }

            // Check possible matches vertically
            // TODO: reduce duplication of column scans
            List<PixelScanResult> secondPass = new List<PixelScanResult>();
            foreach (var result in firstPass)
            {
                if( !secondPass.Any(x => Math.Abs(x.X - result.X) < result.Size/4 && Math.Abs(x.Y - result.Y) < result.Size / 4))
                {
                    List<PixelScanResult> temp = new List<PixelScanResult>();
                    scanner.ScanLine(state.Pixels, (int)result.X, 0, 0, 1, state.Width - 1, state.Height - 1, temp);
                    foreach (var r2 in temp)
                    {
                        if(Math.Abs(r2.Y - result.Y) < result.Size / 4 && result.Size < r2.Size * (1.0f + Variance) && r2.Size < result.Size * (1.0f + Variance))
                        {
                            secondPass.Add(r2);
                        }
                    }
                }
            }

            // Verify with diagonal scan
            List<PixelScanResult> thirdPass = new List<PixelScanResult>();
            foreach (var result in secondPass)
            {
                int offset = Math.Min((int)result.X, (int)result.Y);
                List<PixelScanResult> temp = new List<PixelScanResult>();
                scanner.ScanLine(state.Pixels, (int)result.X - offset, (int)result.Y - offset, 1, 1, state.Width - 1, state.Height - 1, temp);
                if( temp.Any(x => Math.Abs(x.X - result.X) < x.Size/4 && Math.Abs(x.Y - result.Y) < x.Size / 4 && result.Size < x.Size * (1.0f + Variance) && x.Size < result.Size * (1.0f + Variance)))
                {
                    thirdPass.Add(result);
                }
            }

            return thirdPass.Select(x => new Vector2(x.X, x.Y)).ToArray();
        }

        private void ProcessAnchors(ScanState state)
        {
            state.TopLeftAnchor = Vector2.Zero;
            state.RightAnchor = Vector2.Zero;
            state.BottomAnchor = Vector2.Zero;
            if (state.AnchorPoints.Length != 3) return;

            int tlIndex = 0;
            int rIndex = 1;
            int bIndex = 2;
            // Calculate the dot products to the other points for each point
            Vector2 normal01 = Vector2.Normalize(state.AnchorPoints[1] - state.AnchorPoints[0]);
            Vector2 normal02 = Vector2.Normalize(state.AnchorPoints[2] - state.AnchorPoints[0]);
            Vector2 normal12 = Vector2.Normalize(state.AnchorPoints[2] - state.AnchorPoints[1]);
            float[] dots = new float[] { Vector2.Dot(normal01, normal02), Vector2.Dot(-normal01, normal12), Vector2.Dot(-normal02, -normal12) };
            if (dots[1] < dots[tlIndex]) { tlIndex = 1; rIndex = 0; bIndex = 2; }
            if (dots[2] < dots[tlIndex]) { tlIndex = 2; rIndex = 0; bIndex = 1; }

            // TL is the one with the max angle (min dot product) between the vectors to the other points
            state.TopLeftAnchor = state.AnchorPoints[tlIndex];

            // Swap the other two, if the bottom anchor isn't clockwise from the right anchor (relative to the TL anchor) 
            Vector2 v1 = state.AnchorPoints[rIndex] - state.AnchorPoints[tlIndex];
            Vector2 v2 = state.AnchorPoints[bIndex]- state.AnchorPoints[tlIndex];
            if (v1.Y * v2.X > v1.X * v2.Y)
            {
                // Swap the points
                int temp = bIndex;
                bIndex = rIndex;
                rIndex = temp;
            }

            state.RightAnchor = state.AnchorPoints[rIndex];
            state.BottomAnchor = state.AnchorPoints[bIndex];
        }

        private void CalculateTimingPoints(ScanState state)
        {
            Vector2 center = (state.RightAnchor + state.BottomAnchor) / 2.0f;
            float stepSize = 0.25f;

            Vector2 centerDirection = Vector2.Normalize(center - state.TopLeftAnchor);
            float currentDistance = 0.0f;
            while(state.Pixels[(int)(state.TopLeftAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.TopLeftAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            while(state.Pixels[(int)(state.TopLeftAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.TopLeftAnchor.X + centerDirection.X * currentDistance)] == 1) currentDistance += stepSize;
            while(state.Pixels[(int)(state.TopLeftAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.TopLeftAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            state.TopLeftTiming = state.TopLeftAnchor + 0.875f * currentDistance * centerDirection;

            centerDirection = Vector2.Normalize(center - state.RightAnchor);
            currentDistance = 0.0f;
            while (state.Pixels[(int)(state.RightAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.RightAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            while (state.Pixels[(int)(state.RightAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.RightAnchor.X + centerDirection.X * currentDistance)] == 1) currentDistance += stepSize;
            while (state.Pixels[(int)(state.RightAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.RightAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            state.RightTiming = state.RightAnchor + 0.875f * currentDistance * centerDirection;

            centerDirection = Vector2.Normalize(center - state.BottomAnchor);
            currentDistance = 0.0f;
            while (state.Pixels[(int)(state.BottomAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.BottomAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            while (state.Pixels[(int)(state.BottomAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.BottomAnchor.X + centerDirection.X * currentDistance)] == 1) currentDistance += stepSize;
            while (state.Pixels[(int)(state.BottomAnchor.Y + centerDirection.Y * currentDistance)][(int)(state.BottomAnchor.X + centerDirection.X * currentDistance)] == 0) currentDistance += stepSize;
            state.BottomTiming = state.BottomAnchor + 0.875f * currentDistance * centerDirection;
        }

        private void CalculateSizeFromTiming(ScanState state)
        {
            List<PixelScanResult> matches = new List<PixelScanResult>();
            Vector2 delta = 0.25f * Vector2.Normalize(state.RightTiming - state.TopLeftTiming);
            TimingScanner.ScanLine(state.Pixels, state.TopLeftTiming.X, state.TopLeftTiming.Y, delta.X, delta.Y, state.RightTiming.X, state.RightTiming.Y, matches);
            int matchCount = matches.Count();

            matches.Clear();
            delta = 0.25f * Vector2.Normalize(state.BottomTiming - state.TopLeftTiming);
            TimingScanner.ScanLine(state.Pixels, state.TopLeftTiming.X, state.TopLeftTiming.Y, delta.X, delta.Y, state.BottomTiming.X, state.BottomTiming.Y, matches);
            matchCount = Math.Max(matchCount, matches.Count());

            // TODO: Verify that matches are all approximately the same size as their neighbours, otherwise something is going wrong

            state.GridSize = 13 + 2 * matchCount;
            Console.WriteLine("Grid width: " + state.GridSize + " squares.");
        }

        private Vector2 GetLocation(ScanState state, int qrX, int qrY)
        {
            // TODO: Caching alignment information
            int keyDiff = state.GridSize - 7;
            Vector2 dx = (state.RightAnchor - state.TopLeftAnchor) / keyDiff;
            Vector2 dy = (state.BottomAnchor - state.TopLeftAnchor) / keyDiff;

            // TODO: Take skew, alignment points into account
            return state.TopLeftAnchor + (qrX - 3) * dx + (qrY - 3) * dy;
        }

        private void ReadCode(ScanState state)
        {
            if( state.QrCode == null || state.QrCode.Length != state.GridSize)
            {
                state.QrCode = new int[state.GridSize][];
                for( int i = 0; i < state.GridSize; i++)
                {
                    state.QrCode[i] = new int[state.GridSize];
                }
            }

            for( int y = 0; y < state.GridSize; y++)
            {
                for(int x = 0; x < state.GridSize; x++)
                {
                    Vector2 location = GetLocation(state, x, y);
                    state.QrCode[y][x] = state.Pixels[(int)(location.Y + 0.5f)][(int)(location.X + 0.5f)];
                }
            }
        }
    }
}
