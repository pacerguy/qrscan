﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace QrScanLib
{
    public class ScanState
    {
        public ScanState(int[][] pixels)
        {
            Pixels = pixels;
            Height = pixels.Length;
            Width = 0;
            if( Height > 0)
            {
                Width = pixels[0].Length;
            }
        }

        // Initial state
        public int[][] Pixels { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }

        // Anchor detection stage
        public Vector2[] AnchorPoints { get; internal set; }

        // Anchor information stage
        public Vector2 TopLeftAnchor { get; internal set; }
        public Vector2 RightAnchor { get; internal set; }
        public Vector2 BottomAnchor { get; internal set; }

        // Timing information stage
        public Vector2 TopLeftTiming { get; internal set; }
        public Vector2 RightTiming { get; internal set; }
        public Vector2 BottomTiming { get; internal set; }
        public int GridSize { get; internal set; }

        // Alignment detection stage
        public Vector2[] AlignmentPoints { get; internal set; }

        // The QR code, as read from the pixels
        public int[][] QrCode { get; internal set; }
    }
}
