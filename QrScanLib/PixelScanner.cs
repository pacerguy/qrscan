﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QrScanLib
{
    internal class PixelScanner
    {
        // The pattern to match; alternating black and white
        private float[] PatternMin { get; set; }
        private float[] PatternMax { get; set; }

        private float Variance { get; set; }
        private int InitialPoint { get; set; }

        private int[] States { get; set; }

        public PixelScanner(float[] pattern, int initialPoint, float variance)
        {
            if (pattern == null) throw new ArgumentNullException();
            if (pattern.Length < 1) throw new ArgumentException("Pattern must have at least one element.");
            if (pattern.Any(p => p <= 0.0f)) throw new ArgumentException("Pattern elements must be greater than zero.");
            if (variance <= 0.0f) throw new ArgumentException("Variance must be greater than zero.");

            Variance = variance;
            InitialPoint = initialPoint;
            States = new int[pattern.Length];
            PatternMin = new float[pattern.Length];
            PatternMax = new float[pattern.Length];

            // Normalize the pattern
            float psum = pattern.Sum();
            for (int i = 0; i < pattern.Length; i++)
            {
                PatternMin[i] = (pattern[i] / psum) * (1.0f - Variance);
                PatternMax[i] = (pattern[i] / psum) * (1.0f + Variance);
            }
        }


        public void ScanLine(int[][] pixels, float startX, float startY, float deltaX, float deltaY, float stopX, float stopY, List<PixelScanResult> results)
        {
            int currentState = -1;

            float cx = startX;
            float cy = startY;
            while (true)
            {
                if ((pixels[(int)(cy + 0.5f)][(int)(cx + 0.5f)] == InitialPoint) == (currentState % 2 == 0))
                {
                    if (currentState >= 0)
                    {
                        States[currentState]++;
                    }
                }
                else
                {
                    currentState++;
                    if (currentState < States.Length)
                    {
                        States[currentState] = 1;
                    }
                    else
                    {
                        // Check for match, then continue
                        if (CheckPattern())
                        {
                            int length = States.Sum();
                            //int x1 = cx - length * deltaX;
                            //int y1 = cy - length * deltaY;
                            //int x2 = cx - deltaX;
                            //int y2 = cy - deltaY;
                            float x = cx - (length * deltaX + deltaX) / 2.0f;
                            float y = cy - (length * deltaY + deltaY) / 2.0f;
                            var match = results.FirstOrDefault(m => Math.Abs(m.X - x) < length / 4.0f && Math.Abs(m.Y - y) < length / 4.0f);
                            if (match == null)
                            {
                                results.Add(new PixelScanResult(x, y, length));
                            }
                            else
                            {
                                match.Weight += 1.0f;
                                float ratio = 1.0f / match.Weight;
                                match.X = ratio * x + (1.0f - ratio) * match.X;
                                match.Y = ratio * y + (1.0f - ratio) * match.Y;
                                match.Size = ratio * length + (1.0f - ratio) * match.Size;
                            }
                        }

                        // Continue the search
                        for (int j = 0; j < States.Length - 2; j++)
                        {
                            States[j] = States[j + 2];
                        }

                        currentState -= 2;
                        if (currentState >= 0)
                        {
                            States[currentState] = 1;
                        }
                        else
                        {
                            currentState = -1;
                        }
                    }
                }

                cx += deltaX;
                cy += deltaY;

                if( (deltaX > 0.0f && cx > stopX) || (deltaX < 0.0f && cx < stopX) ||
                    (deltaY > 0.0f && cy > stopY) || (deltaY < 0.0f && cy < stopY))
                {
                    break;
                }
            }
        }

        private bool CheckPattern()
        {
            float length = States.Sum();
            for (int i = 0; i < States.Length; i++)
            {
                float v = (float)States[i] / length;
                if (v < PatternMin[i] || v > PatternMax[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
