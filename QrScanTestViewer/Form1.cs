﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace QrScanTestViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private QrScanLib.ScanState state = null;

        private void testButton_Click(object sender, EventArgs e)
        {
            TestImage("../../test-wikipedia.jpg");
        }

        private void test2Button_Click(object sender, EventArgs e)
        {
            TestImage("../../test-billboard.jpg");
        }

        private void TestImage(string path)
        {
            QrScanLib.Scanner scanner = new QrScanLib.Scanner(0.35f);

            var img = Image.FromFile(path);
            mainImage.Image = img;

            // TODO: Make a helper for this logic
            float brightness = 0.7f;
            int[][] pixels = new int[img.Height][];
            using (Bitmap bmp = new Bitmap(img))
            {
                for (int i = 0; i < img.Height; i++)
                {
                    pixels[i] = new int[img.Width];
                    for (int j = 0; j < img.Width; j++)
                    {
                        pixels[i][j] = (bmp.GetPixel(j, i).GetBrightness() > brightness) ? 1 : 0;
                    }
                }
            }

            state = new QrScanLib.ScanState(pixels);
            scanner.Scan(state);

            mainImage.Refresh();
        }

        private int ScaleXToImage(float x)
        {
            return (int)((x + 0.5f) / (float)mainImage.Image.Width * (float)(mainImage.Width));
        }

        private int ScaleYToImage(float y)
        {
            return (int)((y + 0.5f) / (float)mainImage.Image.Height * (float)(mainImage.Height));
        }

        private void mainImage_Paint(object sender, PaintEventArgs e)
        {
            if( state == null)
            {
                return;
            }

            foreach (var p in state.AnchorPoints)
            {
                int xx = ScaleXToImage(p.X);
                int xy = ScaleYToImage(p.Y);
                e.Graphics.DrawLine(Pens.Red, new Point(xx - 5, xy - 5), new Point(xx + 5, xy + 5));
                e.Graphics.DrawLine(Pens.Red, new Point(xx - 5, xy + 5), new Point(xx + 5, xy - 5));
            }

            foreach (var p in state.AlignmentPoints)
            {
                int xx = ScaleXToImage(p.X);
                int xy = ScaleYToImage(p.Y);
                e.Graphics.DrawLine(Pens.White, new Point(xx - 3, xy - 3), new Point(xx + 3, xy + 3));
                e.Graphics.DrawLine(Pens.White, new Point(xx - 3, xy + 3), new Point(xx + 3, xy - 3));
            }

            e.Graphics.DrawLine(Pens.Green, new Point(ScaleXToImage(state.TopLeftTiming.X), ScaleYToImage(state.TopLeftTiming.Y)), new Point(ScaleXToImage(state.RightTiming.X), ScaleYToImage(state.RightTiming.Y)));
            e.Graphics.DrawLine(Pens.Green, new Point(ScaleXToImage(state.TopLeftTiming.X), ScaleYToImage(state.TopLeftTiming.Y)), new Point(ScaleXToImage(state.BottomTiming.X), ScaleYToImage(state.BottomTiming.Y)));

            e.Graphics.DrawRectangle(Pens.Green, new Rectangle(ScaleXToImage(state.TopLeftAnchor.X) - 10, ScaleYToImage(state.TopLeftAnchor.Y) - 10, 21, 21));
            e.Graphics.DrawRectangle(Pens.Blue, new Rectangle(ScaleXToImage(state.BottomAnchor.X) - 10, ScaleYToImage(state.BottomAnchor.Y) - 10, 21, 21));
            e.Graphics.DrawRectangle(Pens.Red, new Rectangle(ScaleXToImage(state.RightAnchor.X) - 10, ScaleYToImage(state.RightAnchor.Y) - 10, 21, 21));

            e.Graphics.DrawRectangle(Pens.White, new Rectangle(ScaleXToImage(state.TopLeftTiming.X) - 2, ScaleYToImage(state.TopLeftTiming.Y) - 2, 5, 5));
            e.Graphics.DrawRectangle(Pens.White, new Rectangle(ScaleXToImage(state.RightTiming.X) - 2, ScaleYToImage(state.RightTiming.Y) - 2, 5, 5));
            e.Graphics.DrawRectangle(Pens.White, new Rectangle(ScaleXToImage(state.BottomTiming.X) - 2, ScaleYToImage(state.BottomTiming.Y) - 2, 5, 5));
        }
    }
}
